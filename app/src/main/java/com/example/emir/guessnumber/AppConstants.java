package com.example.emir.guessnumber;

public class AppConstants {

    public static final String SHARED_PREFERENCES = "com.example.emir.guessnumber";
    public static final String SHARED_SCORE = "bestScore";
    public static final String SHARED_LANG = "language";
    public static final Integer SHARED_MAX_SCORE = 1000000000;

    public static final String SHARED_SECRET_NUMBER = "secretNumber";

}
