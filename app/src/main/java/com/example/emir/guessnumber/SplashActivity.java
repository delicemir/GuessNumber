package com.example.emir.guessnumber;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends AppCompatActivity {

    @ViewById(resName = "text_splash")
    protected TextView splashText;

    @ViewById(resName = "img_splash")
    protected ImageView imgSplash;

    @ViewById(resName = "main_holder_splash")
    protected LinearLayout mainHolder;


    @AfterViews
    public void init() {
        Animation splashAnimation = AnimationUtils.loadAnimation(this, R.anim.animate);
        mainHolder.startAnimation(splashAnimation);
        startWithDelay();
    }

    @UiThread(delay = 4000)
    public void startWithDelay() {
        startActivity(new Intent(SplashActivity.this, StartupActivity.class));
        finish();
    }
}
