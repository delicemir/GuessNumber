package com.example.emir.guessnumber;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @ViewById(resName = "labTitle")
    protected TextView labTitle;

    @ViewById(resName = "inputNumber")
    protected EditText inputNumber;

    //private Button btnClickMe;
    private CheckBox square;
    private CheckBox even;
    private CheckBox odd;
    private CheckBox prime;

    public int score;
    private int oldScore;
    private int number;
    private int selected;
    private Toast toast;
    private MediaPlayer mp;

    private SharedPreferences sharedPreferences;

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    public int getSelected() {
        return selected;
    }

    @AfterViews
    public void init() {
        square = findViewById(R.id.square);
        even = findViewById(R.id.even);
        odd = findViewById(R.id.odd);
        prime = findViewById(R.id.prime);

        sharedPreferences = this.getSharedPreferences(AppConstants.SHARED_PREFERENCES,MODE_PRIVATE);
        selected = sharedPreferences.getInt(AppConstants.SHARED_LANG,0);
        oldScore = sharedPreferences.getInt(AppConstants.SHARED_SCORE,1000000000);  // old score
        score=0; //new score

        getRandNumber();
        toast=Toast.makeText(MainActivity.this,"",Toast.LENGTH_SHORT);
        mp=MediaPlayer.create(this,R.raw.button_click);
    }

    private void getRandNumber() {
        if(sharedPreferences!=null) {
            setNumber(sharedPreferences.getInt(AppConstants.SHARED_SECRET_NUMBER,0));
            checkHints(getNumber());
        }
        else {
            Toast.makeText(MainActivity.this,R.string.error_startup, Toast.LENGTH_LONG).show();
        }

    }

    private void checkHints(int num){

        if(isSquare(num)){
            square.setChecked(true);
        }
        else {
            square.setChecked(false);
        }
        if(isEven(num)){
            even.setChecked(true);
        }
        else {
            even.setChecked(false);
        }
        if (isOdd(num)){
            odd.setChecked(true);
        }
        else {
            odd.setChecked(false);
        }
        if(isPrime(num)){
            prime.setChecked(true);
        }
        else {
            prime.setChecked(false);
        }

    }

    @Click(resName = "clickMe")
    public void guessNumber(View view) {
        int inputNum=0;
        mp.start();
        if(!toast.getView().isShown()) {
            if (inputNumber.getText().length() == 0) {
                toast = Toast.makeText(MainActivity.this, R.string.input_empty, Toast.LENGTH_SHORT);
                toast.show();
                return;
            }
            inputNum = Integer.valueOf(inputNumber.getText().toString());
            if (inputNum < getNumber()) {
                toast = Toast.makeText(MainActivity.this, R.string.guess_high, Toast.LENGTH_SHORT);
                toast.show();
                score++;
            } else if (inputNum > getNumber()) {
                toast = Toast.makeText(MainActivity.this, R.string.guess_low, Toast.LENGTH_SHORT);
                toast.show();
                score++;
            } else if (inputNum == getNumber()) {
                toast = Toast.makeText(MainActivity.this, R.string.correct, Toast.LENGTH_SHORT);
                toast.show();
                score++;
                saveNewScore(score);
                Intent intent = new Intent(MainActivity.this, StartupActivity.class);
                startActivity(intent);
                finish();
            } else {
                toast = Toast.makeText(MainActivity.this, R.string.error, Toast.LENGTH_SHORT);
                toast.show();
            }
            inputNumber.setText("");
        }
    }

    private void saveNewScore(int score) {
        if(score < oldScore) {
            sharedPreferences.edit().putInt(AppConstants.SHARED_SCORE, score).apply();
        }
    }

    private boolean isSquare(int num) {
        double sqrtNumber = Math.sqrt(num);
        if(sqrtNumber==Math.floor(sqrtNumber)) {
            return true;
        }
        return false;
    }

    private boolean isEven(int num) {
        if(num%2==0){
            return true;
        }
        return false;
    }

    private boolean isOdd(int num){
        if(num%2!=0){
            return true;
        }
        return false;
    }

    private boolean isPrime(int num){
        for(int i=2;i<num;i++) {
            if(num%i==0){
                return false;
            }
        }
        return true;
    }

}
