package com.example.emir.guessnumber;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.Locale;
import java.util.Random;

public class StartupActivity extends AppCompatActivity {

    private Button btnStart;
    private TextView labScore;
    private Spinner languages;

    private Random rnd = new Random();
    private int myNum=0;
    private int selected;
    private int score;

    private MediaPlayer mp;
    private SharedPreferences sharedPreferences;

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = this.getSharedPreferences(AppConstants.SHARED_PREFERENCES,MODE_PRIVATE);
        selected = sharedPreferences.getInt(AppConstants.SHARED_LANG,0);
        setLanguage(selected);

        setContentView(R.layout.activity_startup);

        init();

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                myNum = rnd.nextInt(100) + 1;
                Intent intent = new Intent(StartupActivity.this, MainActivity_.class);
                saveSecretNumber(myNum);
                //intent.putExtra("randomNumber", myNum);
                startActivity(intent);
                finish();
            }
        });

        languages.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                setLanguage(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void saveSecretNumber(int secretNum) {
        sharedPreferences.edit().putInt(AppConstants.SHARED_SECRET_NUMBER,secretNum).apply();
    }

    private void setLanguage(int position) {
        setSelected(position);
        if(position==0){
            setLocale("en");
        }
        else {
            setLocale("ba");
        }
    }

    private void init() {
        btnStart = findViewById(R.id.btnStart);
        labScore = findViewById(R.id.labScore);
        languages = findViewById(R.id.language);

        mp=MediaPlayer.create(this,R.raw.button_click);
        languages.setSelection(selected);

        showLastBestScore();

    }

    private void showLastBestScore() {
        score = sharedPreferences.getInt(AppConstants.SHARED_SCORE,AppConstants.SHARED_MAX_SCORE);
        if(score==AppConstants.SHARED_MAX_SCORE) {
            labScore.setText(R.string.not_played);
        }
        else {
            labScore.setText(String.valueOf(score));
        }
    }

    private void saveLanguageSetting(int selected) {
        sharedPreferences.edit().putInt(AppConstants.SHARED_LANG,selected).apply();
    }

    public void setLocale(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.setLocale(locale);
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveLanguageSetting(selected);
    }

}